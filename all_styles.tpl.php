<?php
/**
 * @file
 * Style panel html.
 */
?>
<table id='mcwe_general_mcwe_simple_styles_pnl'>
  <tr>
    <td></td>
    <td><a href='' class='mcwe_9margin' id='margin-top' onclick='mcwe_prop_panel(this, +1);return false;'>/\</a></td>
    <td></td>
    <td></td>
    <td><a href='' class='mcwe_9padding' id='padding-top' onclick='mcwe_prop_panel(this, +1);return false;'>/\</a></td>
    <td></td>

    <td colspan='2'  class='mcwe_9font_famyly'><span><select name='fontsss' class='select_font_mcwe' id='font-family' type_call='2'  onchange='mcwe_prop_panel2(this);return false;'><?php print $fonts; ?></select></span></td>

    <td style='width:41px;' class='mcwe_9font_size'><input type='text' class='fonct_size_mcwe' name='ll' id='font-size' type_call='1' onchange='mcwe_prop_panel2(this);return false;'/></td>
    <td id1='bold'><a href='' class='mcwe_9style_font' id='font-weight' onclick='mcwe_prop_panel1(this);return false;'><span style='font-weight:bold;'>B</span></a></td>
    <td id1='italic'><a href='' class='mcwe_9style_font' id='font-style' onclick='mcwe_prop_panel1(this);return false;'><span style='font-style:italic;'>I</span></a></td>
    <td id1='underline'><a href='' class='mcwe_9style_font' id='text-decoration' onclick='mcwe_prop_panel1(this);return false;'><span style='text-decoration:underline;'>U</span></a></td>
  </tr>    
  <tr>
    <td><a href='' class='mcwe_9margin' id='margin-left' onclick='mcwe_prop_panel(this, +1);return false;'><</a></td>
    <td>M</td>
    <td><a href='' class='mcwe_9margin' id='margin-right' onclick='mcwe_prop_panel(this, +1);return false;'>></a></td>
    <td><a href='' class='mcwe_9padding' id='padding-left' onclick='mcwe_prop_panel(this, +1);return false;'><</a></td>
    <td>P</td>
    <td><a href='' class='mcwe_9padding' id='padding-right' onclick='mcwe_prop_panel(this, +1);return false;'>></a></td>

    <td  id1='left' ><a href='' class='mcwe_9text_align' id='text-align' onclick='mcwe_prop_panel1(this);return false;'><img width='20' height='20' src='<?php print $path; ?>/images/shortcut/right.jpg' /></a></td>
    <td id1='center' ><a href='' class='mcwe_9text_align' id='text-align' onclick='mcwe_prop_panel1(this);return false;'><img width='20' height='20' src='<?php print $path; ?>/images/shortcut/center.jpg' /></a></td>
    <td  id1='right' ><a href='' class='mcwe_9text_align' id='text-align' onclick='mcwe_prop_panel1(this);return false;'><img width='20' height='20' src='<?php print $path; ?>/images/shortcut/left.jpg' /></a></td>
    <td  id1='justify' ><a href='' class='mcwe_9text_align' id='text-align'  onclick='mcwe_prop_panel1(this);return false;'><img width='20' height='20' src='<?php print $path; ?>/images/shortcut/justify.jpg' /></a></td>
    <td></td>
    <td id1='line-through'><a href='' class='mcwe_9style_font' id='text-decoration' onclick='mcwe_prop_panel1(this);return false;'><span style='text-decoration:line-through;'>abc</span></a></td>
  </tr> 
  <tr>
    <td></td>
    <td><a href='' class='mcwe_9margin' id='margin-bottom' onclick='mcwe_prop_panel(this, +1);return false;'>\/</a></td>
    <td></td>
    <td></td>
    <td><a href='' class='mcwe_9padding' id='padding-bottom' onclick='mcwe_prop_panel(this, +1);return false;'>\/</a></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> 
</table>
<table id='mcwe_add_mcwe_simple_styles_div'>
</table>
<input id='mcwe_simple_add_property' type='button' class='1' value='Add proprety' onclick='mcwe_simple_add_property(this);' />
