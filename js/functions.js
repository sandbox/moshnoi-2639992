/**
 * @file
 * Some basic functions.
 */

(function ($) {
  "use strict";

  /**
   * Implements function to compare to objects.
   *
   * @param {type} compareTo object.
   *
   * @return {Boolean} if equal.
   */
  $.fn.equals = function (compareTo) {
    if (!compareTo || this.length !== compareTo.length) {
      return false;
    }
    for (var i = 0; i < this.length; ++i) {
      if (this[i] !== compareTo[i]) {
        return false;
      }
    }
    return true;
  };

  /*
   * Make a method.
   */
  $.fn.extend({
    myplugin: function (options, arg) {
      if (options && typeof (options) === 'object') {
        options = $.extend({}, $.myplugin.defaults, options);
      }
      this.each(function () {
        new $.myplugin(this, options, arg);
      });
      return;
    }
  });

  /**
   * Method for moving.
   *
   * @param {type} elem elements.
   * @param {type} options op.
   * @param {type} arg argument.
   */
  $.myplugin = function (elem, options, arg) {
    if (options && typeof (options) === 'string') {
      if (options === 'move') {
        move(arg);
      }
      if (options === 'size_change') {
        size_change(arg);
      }
      return;
    }

    /**
     * Moving a panel in the webpage.
     *
     * @param {type} arg description
     */
    function move(arg) {
      var fmouseX = 0;
      var fmouseY = 0;
      var mouseX = 0;
      var mouseY = 0;
      var ok = false;
      $(elem).mousedown(function (e1) {
        var top = $(elem).css('top');
        top = parseInt(top.replace("px", ""));
        var left = $(elem).css('left');
        left = parseInt(left.replace("px", ""));
        fmouseX = e1.pageX - left;
        fmouseY = top - e1.pageY;
        ok = true;
      }).mouseup(function () {
        ok = false;
        $(elem).css('cursor', 'auto');
      });
      $(elem).find('#trage', '#dialog_body').mousedown(function () {
        ok = false;
      });
      $(document).mousemove(function (e) {
        mouseX = e.pageX;
        mouseY = e.pageY;
        if (ok) {
          $(elem).css({'z-index': '10000', 'top': (mouseY + fmouseY).toString() + 'px', 'left': (mouseX - fmouseX).toString() + 'px', 'cursor': 'move'});
        }
      });
    }

    /**
     * Adding arrows to change the number from textbox.
     *
     * @param {type} arg description
     */
    function size_change(arg) {
      var nr;
      var $div = $('<span  id="controls_size_mcwe_contenare"></span>');
      var $div2 = $('<span  id="controls_size_mcwe"></span>');
      var $div1 = $('<span id="list_control_mcwe"></span>');
      $div1.append('<a href="" id="up_control_mcwe" onclick="return false;up_control_mcwe(); ">/\\</a></br>');
      $div1.append('<a href="" id="down_control_mcwe" onclick="return false;down_control_mcwe(); ">\\/</a>');
      $div.append($div2.append($div1));
      $(elem).after($div);
      $('#controls_size_mcwe #list_control_mcwe #up_control_mcwe').each(function () {
        var up = $(this);
        up.click(function () {
          var obbbj = $(this).parent().parent().parent().prev();
          var valoare = obbbj.val();
          if (valoare === "") {
            obbbj.val('1px').change();
          }
          else {
            nr = valoare.split('px');
            nr = parseInt(nr[0]);
            obbbj.val((nr + 1).toString() + 'px').change();
          }
        });
      });

      $('#controls_size_mcwe #list_control_mcwe #down_control_mcwe').each(function () {
        var down = $(this);
        down.click(function () {
          var obbbj = $(this).parent().parent().parent().prev();
          var valoare = obbbj.val();
          if (valoare === "") {
            obbbj.val('-1px').change();
          }
          else {
            nr = valoare.split('px');

            nr = parseInt(nr[0]);
            obbbj.val((nr - 1).toString() + 'px').change();
          }
        });
      });
    }
  };
}(jQuery));
