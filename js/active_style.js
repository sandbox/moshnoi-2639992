/**
 * @file
 * Adding styles from mcwe_simple_datebase.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.mcwe_simple = {
    attach: function (context, settings) {

      var msg = Drupal.settings.mcwe_simple.rez;

      var ob = msg[0];
      var chd = msg[1];
      var st = msg[2];
      for (var i = 0; i < ob.length; i++) {
        var $obj = $(ob[i]);
        if (chd[i] === 1) {
          $obj = $obj.children();
        }
        $obj.css($.parseJSON(st[i]));

      }
    }
  };
}(jQuery));
