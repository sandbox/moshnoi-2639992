/**
 * @file
 * Implements the functionalities of plugin, edit CSS, get Elements.
 */

// Go - is an element from page, not from our plugin.
var go = false;
var ok = false;
// Perp - we click on the parent/child buttons.
var perp = false;
var obj = {};
var parent = {};
var chd = {};
var style = {};
var get_object = {};
var disable_style = {};
var mcwe_base_url;

(function ($) {
  'use strict';
  Drupal.behaviors.mcwe_simple_block = {
    attach: function (context, settings) {

      mcwe_base_url = $('#mcwe_base_url').text();

      $('#mcwe_simple_add_mcwe_simple_styles_panel #mcwe_general_mcwe_simple_styles_pnl input#font-size').myplugin('size_change');

      // Imphasize the section on the styles panel.
      $('#top_panel li').click(function () {
        $('#top_panel li').css('background-color', 'transparent');
        $(this).css('background-color', 'black');
      });

    }
  };
})(jQuery);

/**
 * Turn off the plugin.
 */
function turn_off_mcwe() {
  'use strict';
  go = false;
  ok = false;
  (function ($) {
    $('#mcwe_simple_process_panel').hide();
    $(obj.path).removeClass('mouse_over').removeClass('get_element');
    $('#turn_mcwe').remove();
    $('#mcwe_div').append('<input id="turn_mcwe" type="button" value="Activate" onclick="turn_on_mcwe();" />');
  })(jQuery);
}

/**
 * Turn on the plugin.
 */
function turn_on_mcwe() {
  'use strict';
  go = true;
  ok = true;
  (function ($) {
    $('#turn_mcwe').remove();
    $('#mcwe_simple_process_panel').show();
    $('#mcwe_div').append('<input id="turn_mcwe" type="button" value="Dezactivate" onclick="turn_off_mcwe();" />');
    mcwe_simple_info_element();
  })(jQuery);
}

/**
 * Tracking the clicking elements.
 */
function mcwe_simple_info_element() {
  'use strict';
  (function ($) {
    // We are not on the page.
    $('#mcwe_simple_process_panel,#mcwe_div').mouseover(function () {
      go = false;
    });
    $('#mcwe_simple_process_panel,#mcwe_div').mouseout(function () {
      if (ok) {
        go = true;
      }
    });
    $('*').click(function (event) {
      if (go) {
        mcwe_find_element(event.target);
        event.stopPropagation();
        return false;
      }
    });

  })(jQuery);
}

/**
 * Find information about element and writing it on the panel.
 *
 * @param {type} el element.
 */
function mcwe_find_element(el) {
  'use strict';

  (function ($) {
    var info = '';
    var $elem = $('#mcwe_simple_process_panel #mcwe_simple_item');
    var $father = $('#mcwe_simple_process_panel #mcwe_simple_father_mcwe_simple_item');
    var $child = $('#mcwe_simple_process_panel #child_mcwe_simple_item');

    $(obj.path).removeClass('mouse_over').removeClass('get_element');

    if (go || perp) {
      $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_mcwe_simple_item_db').empty();
      $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_item_fater_db').empty();
      obj.path = el;
      // If we have an object.
      if ($(obj.path).length) {
        obj.id = $(obj.path).attr('id');

        obj.cl = $(obj.path).attr('class');
        $(el).addClass('mouse_over');

        obj.poz = $(obj.path).index();
        obj.tag = $(obj.path).get(0).tagName;
        info = "<ul>";
        if (obj.id && $(obj.path).length === 1) {
          info += '<li>Id : ' + obj.id + '</li>';
        }
        if (obj.cl && $(obj.path).length === 1) {
          info += '<li>Class : ' + obj.cl + "</li>";
        }
        if ($(obj.path).length === 1) {
          info += '<li>Position : ' + obj.poz + "</li>";
        }
        if ($(obj.path).length > 1) {
          info += "<li>Elements(children):</li>";
        }
        info += '<li>Tag name : ' + obj.tag + "</li>";
        info += '</ul>';
      }
      else {
        info = "no element!!!";
      }
      $elem.html(info);

      parent.path = $(obj.path).parent();
      if ($(parent.path).length) {
        parent.id = $(parent.path).attr('id');
        parent.cl = $(parent.path).attr('class');
        parent.tag = $(parent.path).get(0).tagName;
        info = "<ul>";
        if (parent.id) {
          info += '<li>Id : ' + parent.id + '</li>';
        }
        if (parent.cl) {
          info += '<li>Class : ' + parent.cl + "</li>";
        }
        if (parent.tag) {
          info += '<li>Tag name : ' + parent.tag + "</li>";
        }
        info += '</ul>';
      }
      else {
        info = "No parents!!";
      }
      $father.html(info);

      chd.path = $(obj.path).find(">:first-child");
      if ($(chd.path).length) {
        chd.id = $(chd.path).attr('id');
        chd.cl = $(chd.path).attr('class');
        chd.tag = $(chd.path).get(0).tagName;
        info = "<ul>";
        if (chd.id) {
          info += '<li>Id : ' + chd.id + '</li>';
        }
        if (chd.cl) {
          info += '<li>Class : ' + chd.cl + "</li>";
        }
        if (chd.tag) {
          info += '<li>Tag name : ' + chd.tag + "</li>";
        }
        info += '</ul>';
      }
      else {
        info = "No children!!!";
      }
      $child.html(info);

      mcwe_simple_add_to_db(obj, parent, chd);

      perp = false;
    }

  })(jQuery);

}

/**
 * Adding information about element in Html mcwe_simple_datebase.
 *
 * @param {type} obj object.
 * @param {type} parent parent.
 * @param {type} chd children.
 */
function mcwe_simple_add_to_db(obj, parent, chd) {
  'use strict';
  (function ($) {

    $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_mcwe_simple_item_db').append('<li id="id">' + obj.id + '</li><li id="class">' + obj.cl + '</li><li id="tag">' + obj.tag + '</li><li id="poz">' + obj.poz + '</li>');
    $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_item_fater_db').append('<li id="id">' + parent.id + '</li><li id="class">' + parent.cl + '</li><li id="tag">' + parent.tag + '</li>');

  })(jQuery);
}

/**
 * Check if the element is already in our mcwe_simple_datebase.
 *
 * @param {type} str name of object.
 *
 * @return {Boolean} if aprear more times.
 */
function mcwe_simple_multiple_aquerence(str) {
  'use strict';
  var ok = true;
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements li');
    $tab.each(function () {
      var name = $(this).text();
      if (name === str) {
        ok = false;
      }
    });

  })(jQuery);
  if (ok) {
    return false;
  }
  return true;
}

/**
 * We pressed on get element.
 */
function mcwe_simple_process_element() {
  'use strict';
  var fiu = obj.path;
  (function ($) {

    var lol = '';
    var ifchd = 0;
    if ($(fiu).length > 1) {
      fiu = parent.path;
      ifchd = 1;
    }
    // Getting individual identification string.
    lol = mcwe_simple_recus_geting_tag(fiu);
    if (mcwe_simple_multiple_aquerence(lol)) {
      alert('This element already exist!!!');
      return;
    }

    if (($(fiu).equals($(lol)) && $(lol).length > 0) || (ifchd === 1 && ($(fiu).equals($(lol).children())))) {
      mcwe_simple_add_to_process_elem(lol, ifchd);
      if (ifchd === 0) {
        $(lol).addClass('get_element');
      }
      else {
        $(lol).children().addClass('get_element');
      }
    }
    else {
      alert('No element');
    }

  })(jQuery);
}

/**
 * We go to parrent until we have an unic parent on Html page.
 *
 * @param {type} el element.
 *
 * @return {jqXHR.tagName|String} id.
 */
function mcwe_simple_recus_geting_tag(el) {
  'use strict';
  var call = "";
  var poz;
  var prt;
  var okl = true;
  var tag;

  (function ($) {

    while (okl && $(el).length > 0) {

      poz = (parseFloat($(el).index()) + 1).toString();
      prt = $(el).get(0).tagName;
      var id = $(el).attr('id');
      var cl = $(el).removeClass('mouse_over').attr('class');
      if (cl !== '') {
        cl = mcwe_simple_divide_class(cl);
      }
      tag = $(el).get(0).tagName;
      if (tag === 'BODY' || tag === 'HTML') {
        call = tag + call;
        okl = false;
      }
      else {
        if (cl !== '' && cl !== 'mouse_over' && $(tag + '.' + cl).length === 1) {
          call = tag + '.' + cl + (call !== '' ? ' >' : '') + call;
          okl = false;
        }
        else {
          if (id !== '' && $(tag + '#' + id).length === 1) {
            call = tag + '#' + id + (call !== '' ? ' >' : '') + call;
            okl = false;
          }
          else {
            call = ' ' + tag + ':nth-child(' + poz + ') ' + call;
          }
        }
      }
      el = $(el).parent();
    }
  })(jQuery);

  return call;

}

/**
 * Get all classes of an element.
 *
 * @param {type} cl string of clases.
 *
 * @return {mcwe_simple_divide_class.myArray|String} array of string.
 */
function mcwe_simple_divide_class(cl) {
  'use strict';
  var myArray = cl.split(' ');
  var okl = false;
  var ii = 0;
  (function ($) {
    while (!okl && ii < myArray.length) {
      if ($('.' + myArray[ii]).length === 1) {
        okl = true;
      }
      ii++;
    }

  })(jQuery);
  if (okl) {
    return myArray[ii - 1];
  }
  return '';
}

/**
 * Create an element panel.
 */
function mcwe_make_element() {
  'use strict';
  (function ($) {
    var div = '<div id="mcwe_make_object" style=" outline:none; border: 4px solid #424242;    border-radius: 10px;    color: white;    background-color: #424242; padding:20px;position:fixed;top:30%;left:30%;">';
    div += '<input type="text" style="background-color:white;outline:none;" id="mcwe_mage_object_input" /><br />';
    div += '<input type="button" value="Make object" style="margin: 1px 1px;    font-family: Tahoma;    border: 1px solid black;    border-radius: 6px;    color: white;    box-shadow: 1px 1px rgb(14, 13, 13);    background-color: #424242;" onclick="make_object_submit_mcwe();return false;" />';
    div += '</div>';
    $('body').append(div);
    $('#mcwe_make_object ').myplugin('move');
  })(jQuery);
}

/**
 * Add new created element.
 */
function make_object_submit_mcwe() {
  'use strict';
  (function ($) {
    var val = $('#mcwe_make_object #mcwe_mage_object_input').val();
    $('#mcwe_make_object ').remove();
    go = true;
    ok = true;
    if (val === '' || $(val).length === 0) {
      alert('No valid object!');
      return;
    }
    mcwe_simple_add_to_process_elem(val, 0);
  })(jQuery);
}

/**
 * Add new created element to mcwe_simple_datebase.
 *
 * @param {type} lol object.
 * @param {type} ifchd if has children.
 */
function mcwe_simple_add_to_process_elem(lol, ifchd) {
  'use strict';
  var addcl = '';
  if (ifchd === 1) {
    addcl = 'class="mcwe_simple_haschildren"';
  }
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements');
    var $tabs = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties');
    var $tabss = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe');
    var nr = parseFloat($tab.attr('class'));
    $tab.attr('class', (nr + 1).toString());
    $tab.append('<li ' + addcl + ' id="ll' + nr + '">' + lol + '</li>');
    $tabs.attr('class', (nr + 1).toString());
    $tabs.append('<li ' + addcl + ' id="ll' + nr + '"></li>');
    $tabss.attr('class', (nr + 1).toString());
    var moment_style = JSON.stringify(get_moment_mcwe_simple_styles(lol));
    $tabss.append('<li ' + addcl + ' id="ll' + nr + '">' + moment_style + '</li>');
    $('#add_elements').append('<li ' + addcl + ' id="ll' + nr + '"><span class="add_style_single" onmouseover="add_remarc_class_mcwe(this);" onmouseout="remove_remarc_class_mcwe(this);" onclick="add_mcwe_simple_styles_single(' + nr + ',' + (ifchd === 1 ? 1 : 0) + ');">' + lol + "</span>" + (ifchd === 1 ? '-chd ' : ' ') + ' <a href="" style="float:right;margin-right:10px;" id="delete_el" onclick="delete_el(' + nr + ');return false;"> X</a></li>');

  })(jQuery);
}

/**
 * Get the styles from attribute style.
 *
 * @param {type} name string.
 *
 * @return {unresolved} styles.
 */
function get_moment_mcwe_simple_styles(name) {
  'use strict';
  var s = {};
  (function ($) {
    var css = $(name).attr('style');
    if (css !== '' && css !== ' ' && css !== null && typeof css !== "undefined") {
      css = css.split(";");
      var l;
      for (var i in css) {
        if (!{}.hasOwnProperty.call(css, i)) {
          continue;
        }
        var check = css[i].split("url(");

        if (typeof check[1] === "undefined") {
          l = css[i].split(":");
        }
        else {

          l = css[i].split(":");
          l[1] = l[1] + ':' + l[2];
        }
        if (l[1] !== '' && l[1] !== ' ' && l[1] !== null && typeof l[1] !== "undefined") {
          s[$.trim(l[0])] = [1, ($.trim(l[1]))];
        }
      }
    }
  })(jQuery);
  return s;
}

/**
 * Remove remarc class.
 *
 * @param {type} obj object.
 */
function remove_remarc_class_mcwe(obj) {
  'use strict';
  (function ($) {
    $($(obj).text()).removeClass('mouse_over_li');
  })(jQuery);
}

/**
 * Add remarc class.
 *
 * @param {type} obj object.
 */
function add_remarc_class_mcwe(obj) {
  'use strict';
  (function ($) {
    $($(obj).text()).addClass('mouse_over_li');
  })(jQuery);
}

/**
 * Delete element.
 *
 * @param {type} nr number.
 */
function delete_el(nr) {
  'use strict';
  (function ($) {
    $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties li#ll' + nr).remove();
    $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe li#ll' + nr).remove();
    $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements li#ll' + nr).remove();
    $('#add_elements li#ll' + nr).remove();
  })(jQuery);
}

// Go throw HTML.
/**
 * Process parent.
 */
function mcwe_simple_mcwe_simple_process_parent() {
  'use strict';
  perp = true;
  mcwe_find_element(parent.path);
}

/**
 * Procces child.
 */
function mcwe_simple_process_chd() {
  'use strict';
  perp = true;
  mcwe_find_element(chd.path);
}

/**
 * Process children.
 */
function mcwe_simple_process_chds() {
  'use strict';
  (function ($) {
    if ($(obj.path).children().length > 0) {
      perp = true;
      mcwe_find_element($(obj.path).children());
    }
  })(jQuery);
}

/**
 * Go to the previous element.
 */
function mcwe_simple_mcwe_simple_process_up_elem() {
  'use strict';
  (function ($) {
    if ($(obj.path).next().length > 0) {
      perp = true;
      mcwe_find_element($(obj.path).next());
    }
  })(jQuery);

}

/**
 * Go to the next element.
 */
function mcwe_simple_process_down_elem() {
  'use strict';
  (function ($) {
    if ($(obj.path).prev().length > 0) {
      perp = true;
      mcwe_find_element($(obj.path).prev());
    }
  })(jQuery);

}

/**
 * Go to the parent.
 */
function mcwe_simple_process_up_parent() {
  'use strict';
  (function ($) {
    var $father = $('#mcwe_simple_process_panel #mcwe_simple_father_mcwe_simple_item');
    parent.path = $(parent.path).parent();
    var info;
    if ($(parent.path).length) {
      parent.id = $(parent.path).attr('id');
      parent.cl = $(parent.path).attr('class');
      parent.tag = $(parent.path).get(0).tagName;
      info = "<ul>";
      if (parent.id) {
        info += '<li>Id : ' + parent.id + '</li>';
      }
      if (parent.cl) {
        info += '<li>Class : ' + parent.cl + "</li>";
      }
      if (parent.tag) {
        info += '<li>Tag name : ' + parent.tag + "</li>";
      }
      info += '</ul>';
    }
    else {
      info = "No parents!!";
    }
    $father.html(info);
    mcwe_simple_add_to_db(obj, parent, chd);
  })(jQuery);

}

// Go throw HTML end.
// Add styles.
/**
 * Click on the objects from the objects list.
 *
 * @param {type} lol - number of li.
 * @param {type} s - if has children.
 */
function add_mcwe_simple_styles_single(lol, s) {
  'use strict';
  go = false;
  ok = false;

  (function ($) {

    $('#top_panel li').css('background-color', 'transparent');
    $('#top_panel ul:nth-child(1) li:nth-child(1)').css('background-color', 'black');
    $(obj.path).removeClass('mouse_over').removeClass('get_element');
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements li#ll' + lol);
    $('#add_elements li').removeClass('process');
    get_object = {};
    $('#add_elements li#ll' + lol).addClass('process');
    get_object = $($tab.text());
    if (s === 1) {
      get_object = get_object.children();
    }
    style = {};
    if (get_object.length > 0) {
      var fr;
      var sec;
      var thrd;
      var scolor = get_object.css('background-color');
      var alb;
      var my;
      var chdsa = scolor.split('rgba');
      if (scolor !== 'transparent' && chdsa[1] === null) {
        my = scolor.split('(');
        my = my[1].split(',');
        alb = my[3];
        fr = my[0];
        sec = my[1];
        thrd = my[2].split(')');
        thrd = thrd[0];
        fr = parseInt(fr);
        sec = parseInt(sec);
        thrd = parseInt(thrd);
        if (typeof alb !== "undefined" && alb !== null) {
          fr = 255;
          sec = 255;
          thrd = 255;
        }
      }

      scolor = get_object.css('color');
      chdsa = scolor.split('rgba');
      if (scolor !== 'transparent' && chdsa[1] === null) {
        my = scolor.split('(');
        my = my[1].split(',');
        alb = my[3];
        fr = my[0];
        sec = my[1];
        thrd = my[2].split(')');
        thrd = thrd[0];
        fr = parseInt(fr);
        sec = parseInt(sec);
        thrd = parseInt(thrd);
        if (typeof alb !== "undefined" && alb !== null) {
          fr = 255;
          sec = 255;
          thrd = 255;
        }
      }
      $('#mcwe_simple_add_mcwe_simple_styles_panel input[type=text]').val("");
      $('#mcwe_simple_add_mcwe_simple_styles_panel #mcwe_general_mcwe_simple_styles_pnl input#font-size').val(parseInt(get_object.css('font-size')));

      $('#mcwe_simple_add_mcwe_simple_styles_panel').show().myplugin('move');

      $('#mcwe_simple_add_mcwe_simple_styles_panel input[type=checkbox]').attr('checked', 'checked');

      mcwe_simple_load_category_properties_section();

      get_object.css(style);
    }
  })(jQuery);
  mcwe_simple_load_mcwe_simple_manual_style();
  mcwe_simple_show_section_category('add_inline_mcwe_simple_styles');
}

/**
 * Process all objects from the list of objects.
 */
function add_mcwe_simple_styles() {
  'use strict';
  go = false;
  ok = false;
  (function ($) {
    $('#top_panel li').css('background-color', 'transparent');
    $('#top_panel ul:nth-child(1) li:nth-child(1)').css('background-color', 'black');
    $('#add_elements li').addClass('process');

    $(obj.path).removeClass('mouse_over').removeClass('get_element');
    var ii = 0;
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements');
    get_object = $();
    var obb;
    $tab.find('li').each(function () {
      ii++;
      if ($(this).attr('class') !== '') {
        obb = $($(this).text()).children();
      }
      else {
        obb = $($(this).text());
      }

      get_object = get_object.add(obb);
    });
    style = {};
    if (ii > 0) {

      $('#mcwe_simple_add_mcwe_simple_styles_panel input[type=text]').val("");
      $('#mcwe_simple_add_mcwe_simple_styles_panel #mcwe_general_mcwe_simple_styles_pnl input#font-size').val(parseInt(get_object.css('font-size')));
      $('#mcwe_simple_add_mcwe_simple_styles_panel').show().myplugin('move');
      $('#mcwe_simple_add_mcwe_simple_styles_panel input[type=checkbox]').attr('checked', 'checked');

      mcwe_simple_load_category_properties_section();

      if (style['background-color'] !== null && typeof style['background-color'] !== "undefined") {
        var fr;
        var sec;
        var thrd;
        var scolor = get_object.css('background-color');
        var my = scolor.split('(');
        my = my[1].split(',');
        var alb = my[3];
        fr = my[0];
        sec = my[1];
        thrd = my[2].split(')');
        thrd = thrd[0];
        fr = parseInt(fr);
        sec = parseInt(sec);
        thrd = parseInt(thrd);
        if (typeof alb !== "undefined" && alb !== null) {
          fr = 255;
          sec = 255;
          thrd = 255;
        }
      }
    }
  })(jQuery);
  mcwe_simple_load_mcwe_simple_manual_style();
  mcwe_simple_show_section_category('add_inline_mcwe_simple_styles');
}

/**
 * Load the styles from html mcwe_simple_datebase in the panel style.
 */
function mcwe_simple_load_mcwe_simple_manual_style() {
  'use strict';
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe');
    var ar = {};
    var s = {};
    var ii = 0;
    var stl;
    var stll;
    var cl = 1;
    var dz = {};
    $('#mcwe_add_mcwe_simple_styles_div').empty();
    get_object.each(function () {
      var nr = mcwe_simple_find_nr_db($(this));
      stl = $tab.find('li#' + nr).text();
      if (stl === '' || stl === null || typeof stl === "undefined") {
        stll = {};
      }
      else {
        stll = $.parseJSON(stl);
      }
      $.each(stll, function (key, value) {
        if (s[key] !== null && typeof s[key] !== "undefined" && (s[key] !== stll[key][1] || dz[key] !== stll[key][0])) {
          ar[key] = -1;
        }
        if (!isNumeric(ar[key])) {
          ar[key] = 1;
        }
        else {
          ar[key]++;
        }
        s[key] = stll[key][1];
        dz[key] = stll[key][0];
      });
      ii++;
    });
    $.each(ar, function (key, value) {
      if (value === ii) {
        var $td1 = $('<td></td>');
        var $td2 = $('<td></td>');
        var $td3 = $('<td></td>');
        var $td4 = $('<td></td>');
        $('#mcwe_add_mcwe_simple_styles_div #mcwe_simple_add_property').attr('class', cl.toString());
        var $div = $('<tr id="mcwe_nivel' + (cl.toString()) + '"></tr>');
        $td1.append("<input type='checkbox' name='mcwe_' checked='checked' value='mcwe_' class='mcwe' onchange='mcwe_simple_disable_propriety_general(this.checked,this);'>");
        $div.append($td1);
        $td2.addClass('prop_mcwe').html("<input type='text' onkeyup='auto_mcwe_simple_styles_general(this.value,this);'  name='styles'/>");
        $div.append($td2);
        $td3.addClass('value_mcwe').append("<input type='text' onchange='set_mcwe_simple_manual_style(this);' onkeyup='mcwe_simple_auto_vll_general(this.value,this);'  name='ll'/>");
        $div.append($td3);
        $td4.addClass('value_mcwe_close').html("<a href='' onclick='return false;'>X</a>");
        $div.append($td4);
        $('#mcwe_add_mcwe_simple_styles_div').append($div);

        $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + (cl.toString()) + ' .prop_mcwe input[type=text]').val(key);
        $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + (cl.toString()) + ' .value_mcwe input[type=text]').val(s[key]).attr('name', key);
        if (dz[key] === 0) {
          $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + (cl.toString()) + ' input[type=checkbox]').attr('checked', false);
          $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + (cl.toString()) + ' .prop_mcwe input[type=text]').attr('readonly', true);
          $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + (cl.toString()) + ' .value_mcwe input[type=text]').attr('readonly', true);
        }
        cl++;
      }
    });

  })(jQuery);
}

/**
 * Close panel styles.
 */
function mcwe_simple_close_panel_mcwe_simple_styles() {
  'use strict';
  go = true;
  ok = true;
  (function ($) {
    $('#mcwe_simple_add_mcwe_simple_styles_panel').hide();
    $('#add_elements li').removeClass('process');
  })(jQuery);
}

/**
 * Show general style section from style panel.
 *
 * @param {type} id1 id
 */
function mcwe_simple_show_section_category(id1) {
  'use strict';
  (function ($) {
    $('#mcwe_simple_add_mcwe_simple_styles_panel #mcwe_simple_sections_body > div').hide();
    mcwe_simple_load_category_properties_section();
    mcwe_simple_load_mcwe_simple_manual_style();
    if ($(id1).length > 0) {
      $(id1).show();
    }
    else {
      $('#' + id1).show();
    }
  })(jQuery);
}

/**
 * Onchange on the textbox(property) from styles section from style panel.
 *
 * @param {type} value val.
 * @param {type} prop property.
 */
function mcwe_simple_process_propriety(value, prop) {
  'use strict';
  style[prop] = (value);
  (function ($) {
    get_object.css(style);
    get_object.each(function () {
      var nr = mcwe_simple_find_nr_db($(this));
      var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe li#' + nr);
      var stl = $tab.text();
      var stll = {};
      if (stl === '' || stl === null) {
        stll = {};
      }
      else {
        stll = $.parseJSON(stl);
      }
      stll[prop] = [1, style[prop]];
      $tab.text(JSON.stringify(stll));
    });
  })(jQuery);
}

/**
 * Set the CSS property to the process object.
 *
 * @param {type} prop property.
 * @param {type} val value.
 */
function mcwe_simple_manual_style(prop, val) {
  'use strict';
  (function ($) {
    get_object.each(function () {
      var nr = mcwe_simple_find_nr_db($(this));
      var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe li#' + nr);
      var stl = $tab.text();
      var stll = {};
      if (stl === '' || stl === null) {
        stll = {};
      }
      else {
        stll = $.parseJSON(stl);
      }
      stll[prop] = [1, val];

      style[prop] = val;
      $tab.text(JSON.stringify(stll));
      get_object.css(style);

    });
  })(jQuery);
}

/**
 * Set the value for the panel style properties.
 */
function mcwe_simple_load_category_properties_section() {
  'use strict';
  var s1 = {};
  var nr;
  var ob = get_object;
  (function ($) {
    var s = {};
    var ar = {};
    var ii = 0;
    var d = {};
    var ad = {};
    ob.each(function () {
      ii++;
      var css = $(this).attr('style');
      if (css !== '' && css !== ' ' && css !== null && typeof css !== "undefined") {
        css = css.split(";");

        for (var i in css) {
          if (!{}.hasOwnProperty.call(css, i)) {
            continue;
          }
          var check = css[i].split("url(");
          var l;
          if (typeof check[1] === "undefined") {
            l = css[i].split(":");
          }
          else {

            l = css[i].split(":");
            l[1] = l[1] + ':' + l[2];
          }
          if (l[1] !== '' && l[1] !== ' ' && l[1] !== null && typeof l[1] !== "undefined") {
            if (s[$.trim(l[0])] !== null && typeof s[$.trim(l[0])] !== "undefined" && s[$.trim(l[0])] !== $.trim(l[1])) {
              ar[$.trim(l[0])] = -1;
            }
            s[$.trim(l[0])] = ($.trim(l[1]));

            if (!isNumeric(ar[$.trim(l[0])])) {
              ar[$.trim(l[0])] = 1;
            }
            else {
              ar[$.trim(l[0])]++;

            }
          }
        }
      }

      nr = mcwe_simple_find_nr_db($(this));
      disable_style = {};
      disable_style = mcwe_simple_get_disable_stl(nr);
      $.each(disable_style, function (key, value) {
        if (d[key] !== null && typeof d[key] !== "undefined" && d[key] !== value) {
          ad[key] = -1;
        }
        d[key] = value;
        if (!isNumeric(ad[key])) {
          ad[key] = 1;
        }
        else {
          ad[key]++;
        }
      });
    });
    var nr_stl = 0;
    // Disable styles.
    $.each(ad, function (key, value) {
      if (value === ii) {
        if ($('#mcwe_simple_add_mcwe_simple_styles_panel input#' + key).length > 0) {
          $('#mcwe_simple_add_mcwe_simple_styles_panel input#' + key).val(d[key]).attr('readonly', true);
        }
        if ($('#mcwe_simple_add_mcwe_simple_styles_panel input.' + key).length > 0) {
          $('#mcwe_simple_add_mcwe_simple_styles_panel input.' + key).attr('checked', false);
        }

        nr_stl++;
      }
    });

    s1 = {};
    // Styles.
    $.each(ar, function (key, value) {
      if (value === ii) {
        s1[key] = s[key];
        nr_stl++;
        if ($('#mcwe_simple_add_mcwe_simple_styles_panel input#' + key).length > 0) {

          $('#mcwe_simple_add_mcwe_simple_styles_panel input#' + key).val(s[key]);
        }

      }
    });

    $('#mcwe_simple_add_mcwe_simple_styles_panel #mcwe_simple_add_property').attr('class', nr_stl.toString());

  })(jQuery);

  style = s1;
}

/**
 * Is numeric.
 *
 * @param {type} n number.
 *
 * @return {Boolean} if it is number.
 */
function isNumeric(n) {
  'use strict';
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Click on the checkbox from general style section from style panel.
 *
 * @param {type} elem element.
 * @param {type} obj object.
 */
function mcwe_simple_disable_propriety_general(elem, obj) {
  'use strict';
  (function ($) {
    var nr;
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe ');
    var the_stl;
    var stl;
    var style1 = {};
    var stll = {};
    var prop;
    prop = $("#mcwe_add_mcwe_simple_styles_div #" + $(obj).parent().parent().attr('id') + " input[name=styles]").val();
    if (elem) {
      get_object.each(function () {
        the_stl = $(this).attr('style');

        $(this).attr('style', '');
        nr = mcwe_simple_find_nr_db($(this));
        stl = $tab.find('li#' + nr).text();
        if (stl === '' || stl === null) {
          stll = {};
        }
        else {
          stll = $.parseJSON(stl);
          stll[prop][0] = 1;
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }
        disable_style = mcwe_simple_get_disable_stl(nr);
        style[prop] = disable_style[prop];
        delete disable_style[prop];
        insert_disable_prop(nr, disable_style);
        style1 = mcwe_simple_fusion_disable_properties(the_stl, disable_style, prop, elem);
        $(this).css(style1);
      });

      $('#mcwe_simple_add_mcwe_simple_styles_panel #' + prop).add($("#mcwe_add_mcwe_simple_styles_div #" + $(obj).parent().parent().attr('id') + " input[type=text]")).attr('readonly', false);

    }
    else {
      $('#mcwe_simple_add_mcwe_simple_styles_panel #' + prop).add($("#mcwe_add_mcwe_simple_styles_div #" + $(obj).parent().parent().attr('id') + " input[type=text]")).attr('readonly', true);

      get_object.each(function () {

        the_stl = $(this).attr('style');
        $(this).attr('style', "");
        nr = mcwe_simple_find_nr_db($(this));
        stl = $tab.find('li#' + nr).text();
        if (stl === '' || stl === null) {
          stll = {};
        }
        else {
          stll = $.parseJSON(stl);
          stll[prop][0] = 0;
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }

        disable_style = mcwe_simple_get_disable_stl(nr);
        disable_style[prop] = style[prop];
        insert_disable_prop(nr, disable_style);
        style1 = mcwe_simple_fusion_disable_properties(the_stl, disable_style, prop, elem);
        $(this).css(style1);
      });

      delete style[prop];
    }
  })(jQuery);
}

/**
 * Click on the checkbox from the simple sections from the style panel.
 *
 * @param {type} elem element.
 * @param {type} prop property.
 */
function mcwe_simple_disable_propriety(elem, prop) {
  'use strict';
  if (style[prop] === null && disable_style[prop] === null) {
    return;
  }
  (function ($) {
    var the_stl = '';
    var nr;
    var style1 = {};
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe ');
    var stl;
    var stll = {};

    if (elem) {
      get_object.each(function () {
        the_stl = $(this).attr('style');

        $(this).attr('style', '');
        nr = mcwe_simple_find_nr_db($(this));
        stl = $tab.find('li#' + nr).text();
        if (stl === '' || stl === null) {
          stll = {};
          stll = [1, style[prop]];
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }
        else {
          stll = $.parseJSON(stl);
          stll[prop][0] = 1;
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }
        disable_style = mcwe_simple_get_disable_stl(nr);
        style[prop] = disable_style[prop];
        delete disable_style[prop];
        insert_disable_prop(nr, disable_style);
        style1 = mcwe_simple_fusion_disable_properties(the_stl, disable_style, prop, elem);
        $(this).css(style1);
      });

      $('#mcwe_simple_add_mcwe_simple_styles_panel #' + prop).attr('readonly', false);
    }
    else {

      $('#mcwe_simple_add_mcwe_simple_styles_panel #' + prop).attr('readonly', true);

      get_object.each(function () {
        the_stl = $(this).attr('style');
        $(this).attr('style', "");
        nr = mcwe_simple_find_nr_db($(this));
        stl = $tab.find('li#' + nr).text();
        if (stl === '' || stl === null) {
          stll = {};
          stll = [0, style[prop]];
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }
        else {
          stll = $.parseJSON(stl);
          stll[prop][0] = 0;
          $tab.find('li#' + nr).text(JSON.stringify(stll));
        }
        disable_style = mcwe_simple_get_disable_stl(nr);
        disable_style[prop] = style[prop];
        insert_disable_prop(nr, disable_style);
        style1 = mcwe_simple_fusion_disable_properties(the_stl, disable_style, prop, elem);
        $(this).css(style1);
      });

      delete style[prop];
    }

  })(jQuery);
}

/**
 * Solution for a problem from the general properties and simple properties.
 *
 * @param {type} stl - styles.
 * @param {type} dsl - disable styles.
 * @param {type} prop - disable propriety.
 * @param {type} elem - element.
 *
 * @return {unresolved} join disable properties.
 */
function mcwe_simple_fusion_disable_properties(stl, dsl, prop, elem) {
  'use strict';

  var st = {};

  (function ($) {

    $.each(style, function (key, value) {
      st[key] = value;
    });

    if (!elem) {
      delete st[prop];
    }
    if (stl !== null && typeof stl !== "undefined" && stl !== '' && stl !== ' ') {
      var css = stl.split(";");
      for (var i in css) {
        if (!{}.hasOwnProperty.call(css, i)) {
          continue;
        }
        var l = css[i].split(":");
        if ((l[1] !== '' && l[1] !== ' ' && l[1] !== null && typeof l[1] !== "undefined") && dsl[$.trim(l[0])] === null && st[$.trim(l[0])] === null) {
          st[$.trim(l[0])] = $.trim(l[1]);
        }
      }
    }
  })(jQuery);

  return st;
}

/**
 * Get the number of row from a object from html mcwe_simple_datebase.
 *
 * @param {type} ob object.
 *
 * @return {unresolved} number.
 */
function mcwe_simple_find_nr_db(ob) {
  'use strict';
  var nr;

  (function ($) {
    var $obs;
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements');
    $tab.find('li').each(function () {
      $obs = $($(this).text());
      if ($(this).attr('class') !== '') {
        $obs = $obs.children();
      }
      var $dd = $(this);
      $obs.each(function () {
        if ($(this).equals(ob)) {
          nr = $dd.attr('id');
        }
      });

    });
  })(jQuery);

  return nr;
}

/**
 * Insert the desiable property in the html mcwe_simple_datebase.
 *
 * @param {type} nr number.
 * @param {type} str style.
 */
function insert_disable_prop(nr, str) {
  'use strict';
  if (str === '' || str === null || str === ' ') {
    return;
  }
  str = JSON.stringify(str);
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties li#' + nr);
    $tab.text(str);
  })(jQuery);
}

/**
 * Get the disable properties from a row from mcwe_simple_datebase html.
 *
 * @param {type} nr number of row.
 *
 * @return {Object|l|mcwe_simple_get_disable_stl.blockAnonym$3} disable styles.
 */
function mcwe_simple_get_disable_stl(nr) {
  'use strict';
  var s = '';
  var l;
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties li#' + nr);
    s = $tab.text();
    l = $.parseJSON(s);
  })(jQuery);

  if (l !== null && typeof l !== "undefined") {
    return l;
  }
  return {};

}

// Add styles end.
// Save style.
/**
 * Show the panel style.
 */
function save_mcwe_simple_styles() {
  'use strict';

  (function ($) {
    if ($('#add_elements').html() !== '') {
      go = ok = false;
      $('#mcwe_simple_save_style_panel').show();
    }
    else {
      alert('No elements');
    }
  })(jQuery);
}

/**
 * Save the styles on mcwe_simple_datebase, the page where the styles are applyed.
 */
function save_page_mcwe_simple_styles() {
  'use strict';
  var ob = [];
  var st = [];
  var ch = [];
  var ii = 0;
  (function ($) {
    // Getting styles from html mcwe_simple_datebase.
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements li');
    $tab.each(function () {
      ob[ii] = $(this).text();
      if ($(this).attr('class') === 'mcwe_simple_haschildren') {
        ch[ii] = 1;
      }
      else {
        ch[ii] = 0;
      }
      st[ii] = mcwe_simple_get_base_style(ob[ii], ch[ii]);
      ii++;
    });

    // Getting the page where the styles are applyed.
    var page = $('#input_page').val();

    if (page === '') {
      alert('Type pages!');
      return;
    }
    // Senting the data, inserting in mysql mcwe_simple_datebase.
    $.ajax({
      type: "POST",
      url: mcwe_base_url,
      data: {ob: ob, ch: ch, st: st, page: page, action: 'save_style_page'},
      dataType: 'json',
      success: function (msg) {
        if (msg) {
          alert('Styles is saved!');
        }
        else {
          alert('Fail!');
        }
        reset_mcwe();
      }
    });

    // Close the panel and reset the the clicking system.
    close_save_page_mcwe_simple_styles();

  })(jQuery);

}

/**
 * Get style of a object.
 *
 * @param {type} obs object.
 * @param {type} ch if children.
 *
 * @return {s} styles.
 */
function mcwe_simple_get_base_style(obs, ch) {
  'use strict';
  var st = {};
  (function ($) {
    var $ob = $(obs);
    var ar = {};
    var s = {};
    var ii = 0;
    if (ch) {
      $ob = $ob.children();
    }
    $ob.each(function () {
      var css = $(this).attr('style');
      if (css !== '' && css !== ' ' && css !== null && typeof css !== "undefined") {
        css = css.split(";");
        for (var i in css) {
          if (!{}.hasOwnProperty.call(css, i)) {
            continue;
          }
          var check = css[i].split("url(");
          var l;
          if (typeof check[1] === "undefined") {
            l = css[i].split(":");
          }
          else {

            l = css[i].split(":");
            l[1] = l[1] + ':' + l[2];
          }
          if (l[1] !== '' && l[1] !== ' ' && l[1] !== null && typeof l[1] !== "undefined") {
            if (s[$.trim(l[0])] !== null && typeof s[$.trim(l[0])] !== "undefined" && s[$.trim(l[0])] !== $.trim(l[1])) {
              ar[$.trim(l[0])] = -1;
            }
            s[$.trim(l[0])] = ($.trim(l[1]));
            if (!isNumeric(ar[$.trim(l[0])])) {
              ar[$.trim(l[0])] = 1;
            }
            else {
              ar[$.trim(l[0])]++;
            }

          }
        }

      }
      ii++;
    });

    $.each(ar, function (key, value) {
      if (value !== ii) {
        delete s[key];
      }
    });

    st = s;

  })(jQuery);
  return st;
}

/**
 * Close the panel and reset the clicking system.
 */
function close_save_page_mcwe_simple_styles() {
  'use strict';
  go = true;
  ok = true;
  (function ($) {
    $('#mcwe_simple_save_style_panel').hide();
  })(jQuery);
}

/* Autocomplate */
var auto = {'background-color': ['red', 'black', 'blue', 'green', 'transparent'],
  'background-repeat': ['repeat', 'repeat-x', 'repeat-y', 'no-repeat', 'initial', 'inherit'],
  'background-image': [''],
  'background-size': ['inherit', 'initial', 'auto'],
  'background-attachment': ['scroll', 'fixed', 'local', 'initial', 'inherit'],
  'background-origin': ['padding-box', 'border-box', 'content-box', 'initial', 'inherit'],
  'background-position': [''],
  'color': ['red', 'black', 'blue', 'green', 'transparent', 'initial', 'inherit'],
  'font-family': [' Arial', 'Verdana', 'Times New Roman', 'Georgia', 'Serif', 'Helvetica', 'Gill', 'New Century Schoolbook', 'sans-serif', '21st Century'],
  'font-style': ['italic', 'oblique', 'normal', 'inherit'],
  'font-weight': ['normal', 'bold', 'lighter', 'bolder', 'inherit'],
  'font-stretch': ['wider', 'narrower', 'ultra-condensed', 'extra-condensed', 'condensed', 'semi-condensed', 'normal', 'semi-expanded', 'expanded', 'extra-expanded', 'ultra-expanded', 'initial', 'inherit'],
  'font-variant': ['normal'],
  'font-size-adjust': ['none', 'inherit'],
  'list-style': ['square'],
  'list-style-type': ['circle', 'none', 'disc', 'square', 'armenian', 'decimal', 'upper-roman', 'upper-latin', 'upper-alpha', 'lower-roman', 'lower-latin'],
  'list-style-image': [],
  'list-style-position': [],
  'margin': ['auto', 'initial', 'inherit'],
  'margin-top': ['auto', 'initial', 'inherit'],
  'margin-bottom': ['auto', 'initial', 'inherit'],
  'margin-left': ['auto', 'initial', 'inherit'],
  'margin-right': ['auto', 'initial', 'inherit'],
  'padding': ['auto', 'initial', 'inherit'],
  'padding-top': ['auto', 'initial', 'inherit'],
  'padding-bottom': ['auto', 'initial', 'inherit'],
  'padding-left': ['auto', 'initial', 'inherit'],
  'padding-right': ['auto', 'initial', 'inherit'],
  'border-radius': [''],
  'outline': [''],
  'border-collapse': ['separate', 'collapse', 'initial', 'inherit'],
  'border': [''],
  'border-bottom': ['initial', 'inherit'],
  'border-bottom-color': [],
  'border-bottom-left-radius': [],
  'border-bottom-right-radius': [],
  'border-top-left-radius': [],
  'border-top-right-radius': [],
  'border-bottom-style': ['none', 'hidden', 'dotted', 'dashed', 'solid', 'double', 'groove', 'ridge', 'inset', 'outset', 'initial', 'inherit'],
  'border-bottom-width': [],
  'border-color': [],
  'border-width': [],
  'border-spacing': [],
  'border-style': ['none', 'hidden', 'dotted', 'dashed', 'solid', 'double', 'groove', 'ridge', 'inset', 'outset', 'initial', 'inherit'],
  'text-decoration': ['none ', 'underline', 'overline', 'line-through', 'initial ', 'inherit'],
  'text-align': ['left', 'right', ' center', 'justify', 'initial', 'inherit'],
  'text-indent': [],
  'text-transform': ['none ', 'capitalize ', 'uppercase ', 'lowercase ', 'initial ', 'inherit'],
  'word-spacing': ['normal', 'initial ', ' inherit'],
  'float': ['left', 'center', 'right'],
  'clear': ['both'],
  'display': ['block', 'inline', 'inline-block', 'inline-table', 'list-mcwe_simple_item', 'run-in', 'table', 'table-caption', 'table-column-group', 'table-header-group', 'table-footer-group', 'table-row-group', 'table-cell', 'table-column', 'table-row', 'none', 'initial', 'inherit'],
  'position': ['static ', 'absolute', ' fixed', 'relative', ' initial ', ' inherit'],
  'top': [],
  'left': [],
  'right': [],
  'bottom': [],
  'visibility': ['visible ', ' hidden ', ' collapse ', ' initial ', ' inherit'],
  'overflow': ['visible ', ' hidden ', ' scroll ', ' auto ', ' initial ', ' inherit'],
  'overflow-x': ['visible ', ' hidden ', ' scroll ', ' auto ', ' initial ', ' inherit'],
  'overflow-y': ['visible ', ' hidden ', ' scroll ', ' auto ', ' initial ', ' inherit'],
  'z-indez': [],
  'clip': ['auto', 'shape', 'initial', 'inherit'],
  'height': [],
  'width': [],
  'max-width': [],
  'max-height': [],
  'min-width': [],
  'min-height': [],
  'caption-side': ['top', 'bottom', 'initial', 'inherit'],
  'empty-cells': ['show', 'hide', 'initial', 'inherit'],
  'table-layout': ['auto', 'fixed'],
  'resize': [' none', 'both', 'horizontal', 'vertical', 'initial', 'inherit'],
  'cursor': ['alias', 'all-scroll', 'auto', 'cell', 'context-menu', 'col-resize', 'copy', 'crosshair', 'default', 'ew-resize', 'help', 'move', 'n-resize', 'ne-resize', 'nesw-resize', 'ns-resize', 'nw-resize', 'wse-resize', 'no-drop', 'none', 'not-allowed', 'pointer', 'progress', 'row-resize', 's-resize', 'se-resize', 'sw-resize', 'text', 'URL', 'vertical-text', 'w-resize', 'wait', 'zoom-in', 'zoom-out', 'initial', 'inherit']};

// Autoclomplete begin.
/**
 * Onkeyup action for inputs from style panel.
 *
 * @param {type} val value.
 * @param {type} obj object.
 */
function mcwe_simple_auto_style(val, obj) {
  'use strict';
  (function ($) {
    $('#autocomplate_mcwe_contanare').remove();
    if (val.length > 0) {
      var $out;
      var prop = $(obj).attr('id');
      if (typeof auto[prop] !== "undefined" && auto[prop] !== null) {

        var ar = auto[prop];
        $out = $('<span id="autocomplate_mcwe"></span>');
        var ol = false;
        for (var i = 0; i < ar.length; i++) {
          ar[i] = $.trim(ar[i]);
          if (ar[i].indexOf(val) === 0) {
            $out.append('<span class="mcwe_simple_items_mawe_autocomplate"><a href="" class="' + prop + '" onclick="mcwe_simple_add_value(this);return false;">' + $.trim(ar[i]) + '</a></span></br>');
            ol = true;
          }
        }
        if (ol) {
          // Add list of sugestions;
          $(obj).parent().append($('<div id="autocomplate_mcwe_contanare"></div>').append($('<span id="autocomplate_mcwe_second"></span>').append($out)));
        }
      }
      // Todo add mouseout.
    }
  })(jQuery);
}

/**
 * Was a click on the autocpmplete list.
 *
 * @param {type} obj object.
 */
function mcwe_simple_add_value(obj) {
  'use strict';
  (function ($) {
    $('#mcwe_simple_add_mcwe_simple_styles_panel #' + $(obj).attr('class')).val($(obj).text()).change();
    if ($(obj).attr('class') === "background-color") {
      var fr;
      var sec;
      var thrd;
      var scolor = get_object.css('background-color');
      var my = scolor.split('(');
      my = my[1].split(',');
      var alb = my[3];
      fr = my[0];
      sec = my[1];
      thrd = my[2].split(')');
      thrd = thrd[0];
      fr = parseInt(fr);
      sec = parseInt(sec);
      thrd = parseInt(thrd);
      if (typeof alb !== "undefined" && alb !== null) {
        fr = 255;
        sec = 255;
        thrd = 255;
      }
    }
    $('#autocomplate_mcwe_contanare').remove();
  })(jQuery);
}

/**
 * Onkeyup action for add property textboxes.
 *
 * @param {type} val the value textbox have
 * @param {type} obj Textbox in we write
 */
function auto_mcwe_simple_styles_general(val, obj) {
  'use strict';
  var kj = true;
  (function ($) {
    $('#autocomplate_mcwe_contanare').remove();
    if (val[val.length - 1] === ':') {
      val = val.split(":");
      val = val[0];
      if (auto[val] !== null || 1) {
        $(obj).val(val);
        mcwe_simple_add_value_general($(obj).parent().parent().attr('id'), val);
      }
      else {
        $(obj).val(val);
      }
    }
    if (val.length > 0 && kj) {
      var $out = $('<span id="autocomplate_mcwe"></span>');
      var ol = true;
      $.each(auto, function (key, value) {
        key = $.trim(key);
        if (key.indexOf(val) === 0) {
          $out.append('<span class="mcwe_simple_items_mawe_autocomplate"><a href="" class="' + $(obj).parent().parent().attr('id') + '" onclick="mcwe_simple_add_general_prop(this);return false;">' + $.trim(key) + '</a></span></br>');
          ol = true;
        }
      });
      if (ol) {
        $(obj).parent().append($('<div id="autocomplate_mcwe_contanare"></div>').append($('<span id="autocomplate_mcwe_second"></span>').append($out)));
      }
    }
  })(jQuery);
}

/**
 * When we click on A sugestion from autocomplete list.
 *
 * @param {type} obj the textbox in we type.
 */
function mcwe_simple_add_general_prop(obj) {
  'use strict';
  (function ($) {
    $('#autocomplate_mcwe_contanare').remove();
    $('#mcwe_add_mcwe_simple_styles_div #' + $(obj).attr('class') + ' .prop_mcwe input[type=text]').val($(obj).text());
    mcwe_simple_add_value_general($(obj).attr('class'), $(obj).text());
  })(jQuery);
}

/**
 * Add property textbox on the general panel style.
 *
 * @param {type} cl - the class of the line.
 * @param {type} prop - the property.
 */
function mcwe_simple_add_value_general(cl, prop) {
  'use strict';
  (function ($) {
    $('#mcwe_add_mcwe_simple_styles_div #' + cl + ' .value_mcwe').html("<input type='text' onchange='set_mcwe_simple_manual_style(this);' onkeyup='mcwe_simple_auto_vll_general(this.value,this);'  name='" + prop + "'/>");
    $('#mcwe_add_mcwe_simple_styles_div #' + cl + ' .value_mcwe input[type=text]').focus();
  })(jQuery);
}

/**
 * Onkeyup from property textbox from general style panel.
 *
 * @param {type} val value.
 * @param {type} obj object.
 */
function mcwe_simple_auto_vll_general(val, obj) {
  'use strict';
  (function ($) {
    $('#autocomplate_mcwe_contanare').remove();
    if (val.length > 0) {
      var prop = $(obj).attr('name');
      var cl = $(obj).parent().parent().attr('id');

      if (typeof auto[prop] !== "undefined" && auto[prop] !== null) {
        var ar = auto[prop];
        var $out = $('<span id="autocomplate_mcwe"></span>');
        var ol = false;
        for (var i = 0; i < ar.length; i++) {
          ar[i] = $.trim(ar[i]);
          if (ar[i].indexOf(val) === 0) {
            $out.append('<span class="mcwe_simple_items_mawe_autocomplate"><a href="' + cl + '" class="' + prop + '" onclick="mcwe_simple_add_value_for_general(this);return false;">' + $.trim(ar[i]) + '</a></span></br>');
            ol = true;
          }
        }
        if (ol) {
          $(obj).parent().append($('<div id="autocomplate_mcwe_contanare"></div>').append($('<span id="autocomplate_mcwe_second"></span>').append($out)));
        }
      }
    }
  })(jQuery);
}

/**
 * Set the style we type on textbox to the process object.
 *
 * @param {type} obj object.
 */
function set_mcwe_simple_manual_style(obj) {
  'use strict';
  (function ($) {
    mcwe_simple_manual_style($(obj).attr('name'), $(obj).val());
  })(jQuery);
}

/**
 * Autocomplete sugestion for property of the general style panel.
 *
 * @param {type} obj object.
 */
function mcwe_simple_add_value_for_general(obj) {
  'use strict';
  (function ($) {
    var prop = $(obj).attr('class');
    var val = $(obj).attr('text');
    var cl = $(obj).attr('href');
    $('#mcwe_add_mcwe_simple_styles_div #' + cl + ' .value_mcwe input[type=text]').val($(obj).text());
    $('#autocomplate_mcwe_contanare').remove();
    mcwe_simple_manual_style(prop, val);

  })(jQuery);
}

/**
 * When clicking on add property buttom from style general panel.
 *
 * @param {type} obj object.
 */
function mcwe_simple_add_property(obj) {
  'use strict';
  (function ($) {
    var cl = parseInt($(obj).attr('class')) + 1;
    var $td1 = $('<td></td>');
    var $td2 = $('<td></td>');
    var $td3 = $('<td></td>');
    var $td4 = $('<td></td>');
    $(obj).attr('class', cl.toString());
    var $div = $('<tr id="mcwe_nivel' + (cl.toString()) + '"></tr>');
    $td1.append("<input type='checkbox' name='mcwe_' checked='checked' value='mcwe_' class='mcwe' onchange='mcwe_simple_disable_propriety_general(this.checked,this);'>");
    $div.append($td1);
    $td2.addClass('prop_mcwe').html("<input type='text' onkeyup='auto_mcwe_simple_styles_general(this.value,this);'  name='styles'/>");
    $div.append($td2);
    $td3.addClass('value_mcwe');
    $div.append($td3);
    $td4.addClass('value_mcwe_close').html("<a href='' onclick='return false;'>X</a>");
    $div.append($td4);
    $('#mcwe_add_mcwe_simple_styles_div').append($div);
  })(jQuery);
}

/**
 * Add property after clicking on the top mcwe_simple_items from general styles panel.
 *
 * @param {type} obj object.
 * @param {type} prop property.
 */
function mcwe_simple_add_property_category(obj, prop) {
  'use strict';
  (function ($) {

    var cl = parseFloat($(obj).attr('class'));
    var $td1 = $('<td></td>');
    var $td2 = $('<td></td>');
    var $td3 = $('<td></td>');
    var $td4 = $('<td></td>');
    cl = cl + 1;
    $(obj).attr('class', cl.toString());
    var $div = $('<tr id="mcwe_nivel' + (cl.toString()) + '"></tr>');
    $td1.append("<input type='checkbox' name='mcwe_' checked='checked' value='mcwe_' class='mcwe' onchange='mcwe_simple_disable_propriety_general(this.checked,this);'>");
    $div.append($td1);
    $td2.addClass('prop_mcwe').html("<input type='text' onkeyup='auto_mcwe_simple_styles_general(this.value,this);'  name='styles'/>");
    $div.append($td2);
    $td3.addClass('value_mcwe').html("<input type='text' onchange='set_mcwe_simple_manual_style(this);' onkeyup='mcwe_simple_auto_vll_general(this.value,this);'  name='" + prop + "'/>");
    $div.append($td3);
    $td4.addClass('value_mcwe_close').html("<a href='' onclick='return false;'>X</a>");
    $div.append($td4);
    $('#mcwe_add_mcwe_simple_styles_div').append($div);

  })(jQuery);
}

/**
 * Padding and margin clicking.
 *
 * @param {type} obj object.
 * @param {type} val value.
 */
function mcwe_prop_panel(obj, val) {
  'use strict';

  (function ($) {
    var prop = $(obj).attr('id');
    if ((typeof style[prop] === "undefined" || style[prop] === null) && (disable_style[prop] === null || typeof disable_style[prop] === "undefined")) {
      var obj_ = '#add_inline_mcwe_simple_styles #mcwe_simple_add_property';
      mcwe_simple_add_property_category(obj_, prop);
      var cll = $(obj_).attr('class');
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .prop_mcwe input[type=text]').val(prop);
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .value_mcwe input[type=text]').val('1px');
      mcwe_simple_manual_style(prop, '1px');
    }
    else {
      var vall = '';
      if (disable_style[prop] === null || typeof disable_style[prop] === "undefined") {
        vall = (transform_px(style[prop]) + parseInt(val)).toString() + 'px';
      }
      else {
        vall = (transform_px(disable_style[prop]) + parseInt(val)).toString() + 'px';
        delete disable_style[prop];
        $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', true);
      }
      mcwe_simple_manual_style(prop, vall);
      $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').val(vall);

    }
  })(jQuery);
}

/**
 * Set properties for text-assign.
 *
 * @param {type} obj object.
 */
function mcwe_prop_panel1(obj) {
  'use strict';

  (function ($) {
    var prop = $(obj).attr('id');
    var val = $(obj).parent().attr('id1');
    if ((style[prop] === null || typeof style[prop] === "undefined") && (disable_style[prop] === null || typeof disable_style[prop] === "undefined")) {

      var obj_ = '#add_inline_mcwe_simple_styles #mcwe_simple_add_property';
      mcwe_simple_add_property_category(obj_, prop);
      var cll = $(obj_).attr('class');
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .prop_mcwe input[type=text]').val(prop);
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .value_mcwe input[type=text]').val(val);

      mcwe_simple_manual_style(prop, val);
    }
    else {
      var chkk = $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked');

      if (style[prop] === val || disable_style[prop] === val) {
        if (chkk) {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', false).change();

        }
        else {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', true).change();

        }
      }
      else {
        if (style[prop] === null || typeof style[prop] === "undefined") {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', true).change();
          mcwe_simple_manual_style(prop, val);
          $('#mcwe_add_mcwe_simple_styles_div  .value_mcwe input[name=' + prop + ']').val(val);
        }
        else {
          mcwe_simple_manual_style(prop, val);
          $('#mcwe_add_mcwe_simple_styles_div  .value_mcwe input[name=' + prop + ']').val(val);
        }
      }
    }
  })(jQuery);
}

/**
 * Set font style.
 *
 * @param {type} obj object.
 */
function mcwe_prop_panel2(obj) {
  'use strict';

  (function ($) {
    var prop = $(obj).attr('id');
    var val = '';
    if ($(obj).attr('type_call') === '2') {
      val = $(obj).find('option:selected').text();
    }
    else {
      val = $(obj).val();
    }
    if ((style[prop] === null || typeof style[prop] === "undefined") && (disable_style[prop] === null || typeof disable_style[prop] === "undefined")) {

      var obj_ = '#add_inline_mcwe_simple_styles #mcwe_simple_add_property';
      mcwe_simple_add_property_category(obj_, prop);
      var cll = $(obj_).attr('class');
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .prop_mcwe input[type=text]').val(prop);
      $('#mcwe_add_mcwe_simple_styles_div #mcwe_nivel' + cll + ' .value_mcwe input[type=text]').val(val);

      mcwe_simple_manual_style(prop, val);
    }
    else {
      var chkk = $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked');

      if (style[prop] === val || disable_style[prop] === val) {
        if (chkk) {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', false).change();

        }
        else {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', true).change();

        }
      }
      else {
        if (style[prop] === null || typeof style[prop] === "undefined") {
          $('#mcwe_add_mcwe_simple_styles_div .value_mcwe input[name=' + prop + ']').parent().parent().find('input[type=checkbox]').attr('checked', true).change();
          mcwe_simple_manual_style(prop, val);
          $('#mcwe_add_mcwe_simple_styles_div  .value_mcwe input[name=' + prop + ']').val(val);
        }
        else {
          mcwe_simple_manual_style(prop, val);
          $('#mcwe_add_mcwe_simple_styles_div  .value_mcwe input[name=' + prop + ']').val(val);
        }
      }
    }
  })(jQuery);

}

/**
 * Transfor from px to numbers.
 *
 * @param {type} val value.
 *
 * @return {unresolved} a int.
 */
function transform_px(val) {
  'use strict';
  var myArray = val.split('px');
  return parseInt(myArray[0]);
}

// Control function and save function.
/**
 * Display control panel with added styles.
 */
function control_save_mcwe_simple_styles() {
  'use strict';

  (function ($) {

    $.ajax({
      type: "POST",
      url: mcwe_base_url + "/?q=mcwe_s_ajax",
      data: "action=get_control_mcwe_simple_styles",
      dataType: 'json',
      success: function (msg) {
        $('#mcwe_style_control #mcwe_table_control').empty();
        $('#mcwe_style_control #mcwe_table_control').append('<tr ><th style="width: 20px;"></th><!--<th>Type</th>--><th>Page</th><th style="width:178px;">User</th><th style="width: 55px;">Date</th><th style="width: 50px;">Options</th></tr>');
        for (var i in msg) {
          if ({}.hasOwnProperty.call(msg, i)) {
            var $tr = $('<tr></tr>');
            $tr.append("<td><input type='checkbox' " + (msg[i]['type'] === '1' ? 'type1="1"' : 'type1="0"') + "  name='mcwe_control' id='" + msg[i]['id'] + "' " + (msg[i]['available'] === '1' ? "checked='checked'" : "") + " onchange='mcwe_simple_control_checkbox_mcwe(this.checked,this);'></td>");
            $tr.append('<td>' + msg[i]['page'] + '</td>');
            $tr.append('<td>' + msg[i]['user'] + '</td>');
            $tr.append('<td>' + msg[i]['date'] + '</td>');
            $tr.append('<td><a href="" id="' + msg[i]['id'] + '" ' + (msg[i]['type'] === '1' ? 'type1="1"' : 'type1="0"') + '   onclick="mcwe_control_edit_mcwe_simple_styles(this);return false;">Edit styles</a><br /><a href="" id="' + msg[i]['id'] + '" ' + (msg[i]['type'] === '1' ? 'type1="1"' : 'type1="0"') + '  onclick="mcwe_go_to_get(this);return false;">Go to get</a></td>');
            $('#mcwe_style_control #mcwe_table_control').append($tr);
          }
        }
      }
    });
    $('#mcwe_style_control').show().myplugin('move');
    go = false;
    ok = false;
  })(jQuery);
}

/**
 * Add added styles to current job.
 *
 * @param {type} obj object.
 */
function mcwe_go_to_get(obj) {
  'use strict';

  (function ($) {
    var type = $(obj).attr('type1');
    var id = $(obj).attr('id');
    if ($('#mcwe_simple_process_panel .mcwe_sesion').attr('id') !== '-1') {
      alert('Already exist editing action!!!');
      return;
    }
    $.ajax({
      type: "POST",
      url: mcwe_base_url + "/?q=mcwe_s_ajax",
      data: "action=mcwe_simple_get_styles_for_current_job&id=" + id + '&type=' + type + "&go_to=1",
      dataType: 'json',
      success: function (msg) {
        var content = "";
        $('#mcwe_simple_process_panel .mcwe_sesion').attr('id', id);
        $('#mcwe_simple_process_panel .mcwe_sesion').attr('type', type);
        content += "<p>Scrie paginile in care vor fi valabile stilurile adaugate separate prin virgula /node/1 , /page/2, pentru toate paginile scrie '*', pentru pagina principala front.</p>";
        content += "<input type='text' id='input_page' value='" + msg['pages'] + "'  name='page' /><br />";
        content += "<input onclick='mcwe_update_in_css_file();return false' value='Update' type='button'/>";
        content += " <input id='close_save_mcwe_simple_styles' type='button' value='Close' onclick='close_save_page_mcwe_simple_styles();' />";

        $('#mcwe_simple_save_style_panel').html(content);
        for (var i in msg['style']) {
          if ({}.hasOwnProperty.call(msg['style'], i)) {
            mcwe_simple_replace_elem_for_editing(msg['object'][i], msg['chd'][i], msg['style'][i], msg['stl'][i]);
          }
        }
      }
    });

  })(jQuery);
}

/**
 * Updating styles in mcwe_simple_datebase after editing them(control get styles).
 */
function mcwe_update_in_css_file() {
  'use strict';
  var ob = [];
  var st = [];
  var ch = [];
  var ii = 0;
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements li');
    $tab.each(function () {
      ob[ii] = $(this).text();
      if ($(this).attr('class') === 'mcwe_simple_haschildren') {
        ch[ii] = 1;
      }
      else {
        ch[ii] = 0;
      }
      st[ii] = mcwe_simple_get_base_style(ob[ii], ch[ii]);
      ii++;
    });

    var page = $('#input_page').val();
    var id = $('#mcwe_simple_process_panel .mcwe_sesion').attr('id');
    var type = $('#mcwe_simple_process_panel .mcwe_sesion').attr('type');
    if (page === '') {
      alert('Type pages!');
      return;
    }
    $('#mcwe_simple_process_panel .mcwe_sesion').attr('id', '-1');
    $('#mcwe_simple_process_panel .mcwe_sesion').attr('type', '-1');

    $('#mcwe_style_control #mcwe_table_control').empty();
    $.ajax({
      type: "POST",
      url: mcwe_base_url,
      data: {ob: ob, ch: ch, st: st, pages: page, action: 'update_style_page', id: id, type: type},
      dataType: 'json',
      success: function (msg) {
        if (msg) {
          alert('Styles is updated!');
        }
        else {
          alert('Fail!');
        }
        var content = "";
        content += "<p>Write pages where the styles will be available!</p>";
        content += "<input type='text' id='input_page' value=''  name='page' /><br />";
        content += "<input id='save_mcwe_simple_styles' type='button' value='SAVE in mcwe_simple_datebase' onclick='save_page_mcwe_simple_styles();' />   ";
        content += " <input id='close_save_mcwe_simple_styles' type='button' value='Close' onclick='close_save_page_mcwe_simple_styles();' />";

        $('#mcwe_simple_save_style_panel').html(content);
        reset_mcwe();
      }
    });

    close_save_page_mcwe_simple_styles();

  })(jQuery);

}

/**
 * Adding element in html mcwe_simple_datebase for future editing(Control-get styles).
 *
 * @param {type} lol object.
 * @param {type} ifchd bool.
 * @param {type} moment_style styles from attribute.
 * @param {type} simple styles all.
 */
function mcwe_simple_replace_elem_for_editing(lol, ifchd, moment_style, simple) {
  'use strict';
  var addcl = '';
  if (ifchd === 1) {
    addcl = 'class="mcwe_simple_haschildren"';
  }
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements');
    var $tabs = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties');
    var $tabss = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe');
    var nr = parseFloat($tab.attr('class'));
    $tab.attr('class', (nr + 1).toString());
    $tab.append('<li ' + addcl + ' id="ll' + nr + '">' + lol + '</li>');
    $tabs.attr('class', (nr + 1).toString());
    $tabs.append('<li ' + addcl + ' id="ll' + nr + '"></li>');
    $tabss.attr('class', (nr + 1).toString());
    $(lol).css($.parseJSON(simple));
    $tabss.append('<li ' + addcl + ' id="ll' + nr + '">' + moment_style + '</li>');
    $('#add_elements').append('<li ' + addcl + ' id="ll' + nr + '"><span class="add_style_single" onmouseover="add_remarc_class_mcwe(this);" onmouseout="remove_remarc_class_mcwe(this);" onclick="add_mcwe_simple_styles_single(' + nr + ',' + (ifchd === 1 ? 1 : 0) + ');">' + lol + "</span>" + (ifchd === 1 ? '-chd ' : ' ') + ' <a href="" style="float:right;margin-right:10px;" id="delete_el" onclick="delete_el(' + nr + ');return false;"> X</a></li>');

  })(jQuery);
}

/**
 * Reset the html database.
 */
function reset_mcwe() {
  'use strict';
  (function ($) {
    var $tab = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_get_elements');
    var $tabs = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #mcwe_simple_element_disable_properties');
    var $tabss = $('#mcwe_div #mcwe_simple_datebase #mcwe_find_element #manual_mcwe_simple_styles_mcwe');
    $tabs.empty().attr('class', '0');
    $tab.empty().attr('class', '0');
    $tabss.empty().attr('class', '0');
    $('#mcwe_simple_save_style_panel #input_page').val("");
    $('#add_elements').empty();

  })(jQuery);
}

/**
 * Editing CSS in the text mode.
 *
 * @param {type} obj object.
 */
function mcwe_control_edit_mcwe_simple_styles(obj) {
  'use strict';
  (function ($) {
    var type = $(obj).attr('type1');
    var id = $(obj).attr('id');
    $.ajax({
      type: "POST",
      url: mcwe_base_url,
      data: "action=mcwe_simple_get_styles_mcwe&id=" + id + '&type=' + type + "&go_to=0",
      dataType: 'json',
      success: function (msg) {
        var content = '<input type="text" name="pages_edit_mcwe_input" value="' + msg['pages'] + '" />';
        content += '<textarea name="mcwe_textaerea_edit" rows="14" cols="40" dir="ltr" id="field_2_3"  tabindex="4">';
        for (var i in msg['style']) {
          if ({}.hasOwnProperty.call(msg['style'], i)) {
            content += msg['object'][i] + '\n';
            content += msg['style'][i] + '\n';
          }
        }
        content += '</textarea><input type="button" value="Save Changes" onclick="edit_mcwe_simple_styles_mcwe_save_input(this);" idmcwe_simple_item="' + id + '" fel="' + type + '">';
        $('#mcwe_see_control_content #mcwe_content_content').html(content);
        $('#mcwe_see_control_content ').show().myplugin('move');
      }
    });

  })(jQuery);
}

/**
 * Save style from the writing mode editing.
 *
 * @param {type} obj object.
 */
function edit_mcwe_simple_styles_mcwe_save_input(obj) {
  'use strict';
  (function ($) {
    var type = $(obj).attr('fel');
    var id = $(obj).attr('idmcwe_simple_item');
    var styles = $(obj).parent().find('textarea').val();
    var pages = $(obj).parent().find('input[type=text]').val();
    $.trim(styles);
    $.trim(pages);
    if (pages === '' || styles === '' || styles === null || typeof styles === "undefined" || typeof pages === "undefined" || pages === null) {
      alert('Completați toate spațiile!');
    }
    else {
      $.ajax({
        type: "POST",
        url: mcwe_base_url,
        data: "action=edit_mcwe_simple_styles_mcwe&id=" + id + '&type=' + type + '&styles=' + styles + '&pages=' + pages,
        dataType: 'json',
        success: function (msg) {
          alert('Changes is saved!');
          close_control_page_mcwe_simple_styles1();
          control_save_mcwe_simple_styles();
        }
      });

    }

  })(jQuery);
}

/**
 * Eneble and Uneble style from control panel.
 *
 * @param {type} val value.
 * @param {type} obj object.
 */
function mcwe_simple_control_checkbox_mcwe(val, obj) {
  'use strict';
  (function ($) {
    var id = $(obj).attr('id');
    if (val === true) {
      $.ajax({
        type: "POST",
        url: mcwe_base_url,
        data: "action=set_true_control_mcwe_simple_styles&val=1&id=" + id,
        dataType: 'json',
        success: function (msg) {
          alert('Changed');
        }
      });
    }
    else {
      $.ajax({
        type: "POST",
        url: mcwe_base_url,
        data: "action=set_true_control_mcwe_simple_styles&val=0&id=" + id,
        dataType: 'json',
        success: function (msg) {
          alert("Changed");
        }
      });
    }
  })(jQuery);
}

/**
 * Close control panel.
 */
function close_control_page_mcwe_simple_styles() {
  'use strict';
  (function ($) {
    $('#mcwe_style_control').hide();
    $('#mcwe_style_control #mcwe_table_control').empty();
    go = true;
    ok = true;
  })(jQuery);
}

/**
 * Close the style panel.
 */
function close_control_page_mcwe_simple_styles1() {
  'use strict';
  (function ($) {
    $('#mcwe_see_control_content').hide();
  })(jQuery);
}

/**
 * Move the panel from one side to another.
 *
 * @param {type} obj object.
 */
function move_mcwe_panel(obj) {
  'use strict';
  (function ($) {
    if ($(obj).text() === '>') {
      $(obj).parent().css({right: '0px', left: 'initial'});
      $(obj).text('<');
    }
    else {
      $(obj).parent().css({left: '0px', right: 'initial'});
      $(obj).text('>');
    }

  })(jQuery);
}
