<?php
/**
 * @file
 * Main div mcwe html.
 */
 ?>
<div id="mcwe_div">
  
      <input id="turn_mcwe" type="button" value="Activate" onclick="turn_on_mcwe();" />
      <?php print $panel; ?>
      <div id="mcwe_simple_datebase">
          <div id="mcwe_find_element">
              <ul id="mcwe_mcwe_simple_item_db"></ul>
              <ul id="mcwe_simple_item_fater_db"></ul>
              <ul id="mcwe_simple_item_child_db"></ul>
              <ul id="mcwe_simple_get_elements" class="0"></ul>
              <ul id="mcwe_simple_element_disable_properties" class="0"></ul>
              <ul id="manual_mcwe_simple_styles_mcwe" class="0"></ul>
          </div>      
      </div>
     
  </div>
