DESCRIPTION
-----------

MCWE – On-line editor CSS style

MCWE is the tool that allows web developers to change the site's design without
 editing CSS files. 


FEATURES
--------

 * Getting elements from html page
 * Editing CSS of HTML elements.
 * Saving CSS in date base
 * Version Control of added style


REQUIREMENTS
------------

This module requires the following modules:

 * Comment (core)


RECOMMENDED MODULES
-------------------


INSTALLATION
------------
 1. Unpack the MCWE_simple  folder and contents in the appropriate modules
 2. Enable the MCWE_simple module in the administration tools.
 

SETUP
-----

To activate MCWE_simple module:

 1. Add MCWE_simple Block using admin dashboard (admin/structure/block)
 2. On the new added block click on Activate button.

Comment administration works the usual way.





KNOWN PROBLEMS AND LIMITATIONS
------------------------------

 * It uses javascript to add CSS(slow performance)
 


MAINTAINERS
-----------

Current maintainers:
* Mosnoi Ion - https://drupal.org/user/2295214
