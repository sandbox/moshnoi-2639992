<?php

/**
 * @file
 * Ajax calls implementation.
 */

/**
 * Implements mcwe_simple_ajax().
 */
function mcwe_simple_ajax() {

  if (empty($_REQUEST['action'])) {
    $_REQUEST['action'] = '';
  }
  global $user;
  $result = '';
  $mcwe_simple_item = 'get_mcwe_simple_process_panel';
  switch ($_REQUEST['action']) {
    case 'mcwe_simple_get_styles_for_current_job':

      $date = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('date'))
          ->condition('nr.id', $_REQUEST['id'])
          ->execute()
          ->fetchField();

      $pages = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('page'))
          ->condition('nr.id', $_REQUEST['id'])
          ->execute()
          ->fetchField();

      $ob = array();
      $ch = array();
      $st = array();

      $rez = db_select('mcwe_s_pages', 'nr')
          ->fields('nr')
          ->condition('nr.date', $date)
          ->execute();

      foreach ($rez as $val) {
        $object = db_select('mcwe_s_objects', 'nr')
            ->fields('nr', array('object', 'chd', 'style_id'))
            ->condition('nr.id', $val->object_id)
            ->execute()
            ->fetchAssoc();

        $ob[] = $object['object'];
        $ch[] = $object['chd'];

        $stl = db_select('mcwe_s_styles', 'nr')
            ->fields('nr', array('style'))
            ->condition('nr.id', $object['style_id'])
            ->execute()
            ->fetchField();

        $st[] = $stl;
      }
      $stll = $st;
      $st = str_replace(",", '],', $st);
      $st = str_replace("}", ']}', $st);
      $st = str_replace(":", ':[1,', $st);
      $result = array(
        'style' => $st,
        'object' => $ob,
        'chd' => $ch,
        'stl' => $stll,
        'pages' => $pages,
        'ok' => isset($_SESSION['mcwe_edit_to_get']),
      );
      if ($_REQUEST['go_to'] == "1" && !isset($_SESSION["mcwe_edit_to_get"])) {
        $_SESSION["mcwe_edit_to_get"] = array(
          'type' => $_REQUEST['type'],
          'id' => $_REQUEST['id'],
        );
      }
      $result['ll'] = $_SESSION["mcwe_edit_to_get"]['type'];
      break;

    case 'mcwe_simple_get_styles_mcwe':
      $date = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('date'))
          ->condition('nr.id', $_REQUEST['id'])
          ->execute()
          ->fetchField();

      $pages = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('page'))
          ->condition('nr.id', $_REQUEST['id'])
          ->execute()
          ->fetchField();

      $ob = array();
      $ch = array();
      $st = array();

      $rez = db_select('mcwe_s_pages', 'nr')
          ->fields('nr')
          ->condition('nr.date', $date)
          ->execute();

      foreach ($rez as $val) {
        $object = db_select('mcwe_s_objects', 'nr')
            ->fields('nr', array('object', 'chd', 'style_id'))
            ->condition('nr.id', $val->object_id)
            ->execute()
            ->fetchAssoc();

        $ob[] = $object['object'];
        $ch[] = $object['chd'];

        $stl = db_select('mcwe_s_styles', 'nr')
            ->fields('nr', array('style'))
            ->condition('nr.id', $object['style_id'])
            ->execute()
            ->fetchField();

        $st[] = $stl;
      }

      $result = array(
        'style' => $st,
        'object' => $ob,
        'chd' => $ch,
        'pages' => $pages,
        'ok' => isset($_SESSION['mcwe_edit_to_get']),
      );
      $result['ll'] = $_SESSION["mcwe_edit_to_get"]['type'];
      break;

    case 'update_style_page':
      $result = $_REQUEST['type'];
      $_SESSION['mcwe_edit_to_get']['id'] = $_REQUEST['id'];
      $styles = $_REQUEST['st'];
      $ob = $_REQUEST['ob'];

      $date = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('date'))
          ->condition('nr.id', $_SESSION['mcwe_edit_to_get']['id'])
          ->execute()
          ->fetchField();

      $objects = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('object_id'))
          ->condition('nr.date', $date)
          ->execute()
          ->fetchAll();

      foreach ($objects as $a) {
        $style_id = db_select('mcwe_s_objects', 'nr')
            ->fields('nr', array('style_id'))
            ->condition('nr.id', $a->object_id)
            ->execute()
            ->fetchField();
        $rez = db_delete('mcwe_s_styles')
            ->condition('id', $style_id)
            ->execute();
        $rez = db_delete('mcwe_s_objects')
            ->condition('id', $a->object_id)
            ->execute();
      }
      $rez = db_delete('mcwe_s_pages')
          ->condition('date', $date)
          ->execute();

      $date = date("Y-m-d H:i:s");
      $page = $_REQUEST['pages'];

      foreach ($styles as $a => $b) {

        $content = '{';
        $nr = 0;
        foreach ($styles[$a] as $t => $tt) {
          $nr++;
          if ($nr != 1) {
            $content .= ',';
          }
          $content .= '"' . $t . '":"' . $tt . '"';
        }

        $content .= '}';

        db_insert('mcwe_s_styles')
            ->fields(array(
              'style' => $content,
              'available' => 1,
            ))
            ->execute();

        $mcwe_simple_item = db_query('SELECT LAST_INSERT_ID()')->fetchField();

        db_insert('mcwe_s_objects')
            ->fields(array(
              'object' => $ob[$a],
              'style_id' => $mcwe_simple_item,
              'chd' => 0,
            ))
            ->execute();

        $object = db_query('SELECT LAST_INSERT_ID()')->fetchField();
        db_insert('mcwe_s_pages')
            ->fields(array(
              'page' => $page,
              'object_id' => $object,
              'available' => 1,
              'date' => $date,
              'user' => $user->uid,
            ))
            ->execute();
      }
      $result = 1;

      unset($_SESSION['mcwe_edit_to_get']);

      break;

    case 'edit_mcwe_simple_styles_mcwe':
      $styless = mcwe_simple_separate_syles_object($_REQUEST['styles']);
      $styles = $styless['stl'];
      $ob = $styless['obj'];
      $date = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('date'))
          ->condition('nr.id', $_REQUEST['id'])
          ->execute()
          ->fetchField();

      $objects = db_select('mcwe_s_pages', 'nr')
          ->fields('nr', array('object_id'))
          ->condition('nr.date', $date)
          ->execute()
          ->fetchAll();

      foreach ($objects as $a) {
        $style_id = db_select('mcwe_s_objects', 'nr')
            ->fields('nr', array('style_id'))
            ->condition('nr.id', $a->object_id)
            ->execute()
            ->fetchField();
        $rez = db_delete('mcwe_s_styles')
            ->condition('id', $style_id)
            ->execute();
        $rez = db_delete('mcwe_s_objects')
            ->condition('id', $a->object_id)
            ->execute();
      }
      $rez = db_delete('mcwe_s_pages')
          ->condition('date', $date)
          ->execute();

      $date = date("Y-m-d H:i:s");
      $page = $_REQUEST['pages'];

      foreach ($styles as $a => $b) {

        db_insert('mcwe_s_styles')
            ->fields(array(
              'style' => $styles[$a],
              'available' => 1,
            ))
            ->execute();
        $mcwe_simple_item = db_query('SELECT LAST_INSERT_ID()')->fetchField();

        db_insert('mcwe_s_objects')
            ->fields(array(
              'object' => $ob[$a],
              'style_id' => $mcwe_simple_item,
              'chd' => 0,
            ))
            ->execute();

        $object = db_query('SELECT LAST_INSERT_ID()')->fetchField();

        db_insert('mcwe_s_pages')
            ->fields(array(
              'page' => $page,
              'object_id' => $object,
              'available' => 1,
              'date' => $date,
              'user' => $user->uid,
            ))
            ->execute();
      }
      break;

    case 'set_true_control_mcwe_simple_styles':

      $table = 'mcwe_s_pages';
      $result = $table;
      db_update($table)
          ->fields(array(
            'available' => $_REQUEST['val'],
          ))
          ->condition('id', $_REQUEST['id'])
          ->execute();
      break;

    case 'get_control_mcwe_simple_styles':
      $rez = db_select('mcwe_s_pages', 'nr')
          ->fields('nr')
          ->execute()
          ->fetchAll();
      $result = array();
      $dates = array();
      foreach ($rez as $val) {
        $user1 = user_load($val->user);
        if (!in_array($val->date, $dates)) {
          $result[] = array(
            'type' => 1,
            'id' => $val->id,
            'page' => $val->page,
            'available' => $val->available,
            'user' => $user1->name,
            'date' => $val->date,
          );
          $dates[] = $val->date;
        }
      }
      break;

    case 'save_style_page':
      $date = date("Y-m-d H:i:s");
      $page = $_REQUEST['page'];

      $ob = $_REQUEST['ob'];
      $ch = $_REQUEST['ch'];
      $st = $_REQUEST['st'];
      foreach ($ob as $a => $b) {

        db_insert('mcwe_s_styles')
            ->fields(array(
              'style' => drupal_json_encode($st[$a]),
              'available' => 1,
            ))
            ->execute();

        $mcwe_simple_item = db_query('SELECT LAST_INSERT_ID()')->fetchField();

        db_insert('mcwe_s_objects')
            ->fields(array(
              'object' => $ob[$a],
              'style_id' => $mcwe_simple_item,
              'chd' => $ch[$a],
            ))
            ->execute();

        $object = db_query('SELECT LAST_INSERT_ID()')->fetchField();
        db_insert('mcwe_s_pages')
            ->fields(array(
              'page' => $page,
              'object_id' => $object,
              'available' => 1,
              'date' => $date,
              'user' => $user->uid,
            ))
            ->execute();
      }

      $result = 1;

      break;
  }
  echo (drupal_json_encode($result));
  module_invoke_all('exit');
  exit;
}

/**
 * Implements mcwe_simple_separate_syles_object().
 */
function mcwe_simple_separate_syles_object($style) {
  $obj = array();
  $stl = array();
  $s = explode('}', $style);
  foreach ($s as $a => $b) {
    $t = explode('{', $s[$a]);
    if ($t[1] != '') {
      $obj[] = trim($t[0]);
      $stl[] = '{' . trim($t[1]) . '}';
    }
  }
  return array('stl' => $stl, 'obj' => $obj);
}
