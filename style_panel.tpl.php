<?php
/**
 * @file
 * Style panel main html.
 */
?>
<div id='mcwe_simple_process_panel'>
    <div id='mcwe_base_url' style='display:none;'><?php print $base_url;?></div>
    <div class='mcwe_sesion' type='-1' id='-1'></div>
    <span style='border: 1px solid black;position:absolute;top:-4px;left:1px;cursor:pointer;padding:2px;background: transparent;border-radius: 12px;text-align: center;' onclick='move_mcwe_panel(this);'>></span>
    <div id='mcwe_simple_object_select'>

        <table>
            <tr>
                <td>
                    <div id='mcwe_simple_father_mcwe_simple_item'>Father</div>
                </td>
                <td id='buttoms'>
                    <input id='mcwe_simple_get_father' type='button' value='Parent' onclick='mcwe_simple_mcwe_simple_process_parent();' />

                </td>
            </tr>
            <tr>
                <td>
                    <div id='mcwe_simple_item'>Element</div>
                </td>
                <td id='buttoms'>  
                    <input id='get_up_mcwe_simple_item' type='button' value='Next' onclick='mcwe_simple_mcwe_simple_process_up_elem();' />
                    <input id='get_down_mcwe_simple_item' type='button' value='Previous' onclick='mcwe_simple_process_down_elem();' />

                </td>
            </tr>
            <tr>
                <td>
                    <div id='child_mcwe_simple_item'>Childs</div>
                </td>
                <td id='buttoms'>
                    <input id='get_chd' type='button' value='1 Child' onclick='mcwe_simple_process_chd();' />
                    <!--<input id='get_chds' type='button' value='Children' onclick='mcwe_simple_process_chds();' />-->

                </td>
            </tr>
        </table>
    </div>
    <input id='mcwe_simple_process_element' type='button' value='Get element' onclick='mcwe_simple_process_element();' />
    <input id='mcwe_simple_process_element' type='button' value='Make element' onclick='mcwe_make_element();' />
    <div id='mcwe_view_elements'>
        <ul id='add_elements'></ul>
    </div>
    <div id='control_bottoms_mcwe'>  
        <!--<input id='add_mcwe_simple_styles' type='button' value='Process all' onclick='add_mcwe_simple_styles();' />-->
        <input id='save_mcwe_simple_styles' type='button' value='SAVE CSS' onclick='save_mcwe_simple_styles();' />
        <input id='save_mcwe_simple_styles' type='button' value='Control' onclick='control_save_mcwe_simple_styles();' />
        <input id='close_mcwe_simple_styles' type='button' value='Close' onclick='turn_off_mcwe();' />
    </div>
    <div id='mcwe_simple_add_mcwe_simple_styles_panel'>
        <div id='top_panel'>
            <ul style='margin:0px;' >
                <li onclick='mcwe_simple_show_section_category(add_inline_mcwe_simple_styles);mcwe_simple_load_mcwe_simple_manual_style();'>General</li>
                <li onclick='mcwe_simple_show_section_category(fon_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Background</li>
                <li onclick='mcwe_simple_show_section_category(font_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Font</li>
                <li onclick='mcwe_simple_show_section_category(text_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Text</li>
                <li onclick='mcwe_simple_show_section_category(border_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Border</li>
            </ul>    
            <ul style='padding-top: 4px;'>    
                <li onclick='mcwe_simple_show_section_category(marimi_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Sizes</li>
                <li onclick='mcwe_simple_show_section_category(cimpuri_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Fields</li>
                <li onclick='mcwe_simple_show_section_category(liste_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Lists</li>
                <li onclick='mcwe_simple_show_section_category(positii_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Position</li>
                <li onclick='mcwe_simple_show_section_category(table_mcwe_simple_styles);mcwe_simple_load_category_properties_section();'>Table</li>

            </ul>
            <a id='close_panel' href='' onclick='mcwe_simple_close_panel_mcwe_simple_styles();return false;'>X</a>
        </div>
        <div id='mcwe_simple_sections_body'>
            <div id='add_inline_mcwe_simple_styles'><?php print $add_mcwe_simple_styles; ?></div>
            <div id='fon_mcwe_simple_styles' ><?php print $fon_mcwe_simple_styles; ?></div>
            <div id='font_mcwe_simple_styles' ><?php print $font_mcwe_simple_styles; ?></div>
            <div id='text_mcwe_simple_styles' ><?php print $text_mcwe_simple_styles; ?></div>
            <div id='border_mcwe_simple_styles' ><?php print $border_mcwe_simple_styles; ?></div>
            <div id='marimi_mcwe_simple_styles' ><?php print $marimi_mcwe_simple_styles; ?></div>
            <div id='cimpuri_mcwe_simple_styles' ><?php print $cimpuri_mcwe_simple_styles; ?></div>
            <div id='liste_mcwe_simple_styles' ><?php print $liste_mcwe_simple_styles; ?></div>
            <div id='positii_mcwe_simple_styles' ><?php print $positii_mcwe_simple_styles; ?></div>
            <div id='table_mcwe_simple_styles' ><?php print $table_mcwe_simple_styles; ?></div>
        </div>
    </div>
    <div id='mcwe_simple_save_style_panel'>
        <p>Write page where you want that style apply, eg. /node/1 , /page/2, for all pages - '*',for front page - front.</p>

        <input type='text' id='input_page'  name='page' /><br />
        <input id='save_mcwe_simple_styles' type='button' value='SAVE in mcwe_simple_datebase' onclick='save_page_mcwe_simple_styles();' />     
        <input id='close_save_mcwe_simple_styles' type='button' value='Close' onclick='close_save_page_mcwe_simple_styles();' />

    </div>
    <div id='mcwe_style_control'>
        <p>Control styles panel</p>
        <table id='mcwe_table_control'>      
        </table>
        <input id='close_control_save_mcwe_simple_styles' type='button' value='close' onclick='close_control_page_mcwe_simple_styles();' />
    </div>
    <div id='mcwe_see_control_content'>
        <div id='mcwe_content_content'></div>
        <input id='close_control_save_mcwe_simple_styles' type='button' value='close' onclick='close_control_page_mcwe_simple_styles1();' />
    </div>

</div>
